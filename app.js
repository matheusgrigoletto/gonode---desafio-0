// Requires
const express = require('express');
const nunjucks = require('nunjucks');
const bodyParser = require('body-parser');
const path = require('path');
const moment = require('moment');

// Server
const app = express();

// Configurar middleware para body-parser
app.use(bodyParser.urlencoded({ extended: false }));

// Configurar View Engine
nunjucks.configure('views', {
  autoescape: true,
  express: app,
});

app.set('view engine', 'njk');
app.set('views', path.join(__dirname, 'views'));

// Pagina inicial
app.get('/', (req, res) => {
  res.render('main');
});

/**
 * Verifica o formato de dtNasc.
 * @param {string} dtNasc
 */
const getDtNasc = (dtNasc) => {
  let format = 'DD/MM/YYYY';

  if (dtNasc[4] === '-') {
    format = 'YYYY-MM-DD';
  }

  return format;
};

// Verificação e redirecionamento
app.post('/check', (req, res) => {
  const { name, dtNasc } = req.body;
  const dtNascFormat = getDtNasc(dtNasc);

  const age = moment().diff(moment(dtNasc, dtNascFormat), 'years');
  let url = '';

  if (age < 18) {
    url = `/minor?nome=${name}`;
  } else {
    url = `/major?nome=${name}`;
  }

  res.redirect(url);
});

// Middleware
app.use(['/major', '/minor'], (req, res, next) => {
  const name = req.query.nome || null;

  if (!name) {
    res.redirect('/');
  } else {
    next();
  }
});

// Página para maiores de 18
app.get('/major', (req, res) => {
  res.render('major', {
    name: req.query.nome,
  });
});

// Página para menores de 18
app.get('/minor', (req, res) => {
  res.render('minor', {
    name: req.query.nome,
  });
});

// Server na porta 3000
app.listen(3000);
